// The MIT License (MIT)
//
// Copyright © 2023 Gregory Oakes
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of
// this software and associated documentation files (the “Software”), to deal in
// the Software without restriction, including without limitation the rights to
// use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
// the Software, and to permit persons to whom the Software is furnished to do so,
// subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
// COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
// IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

const std = @import("std");

// Although this function looks imperative, note that its job is to
// declaratively construct a build graph that will be executed by an external
// runner.
pub fn build(b: *std.Build) void {
    // Standard target options allows the person running `zig build` to choose
    // what target to build for. Here we do not override the defaults, which
    // means any target is allowed, and the default is native. Other options
    // for restricting supported target set are available.
    const target = b.standardTargetOptions(.{});

    // Standard optimization options allow the person running `zig build` to select
    // between Debug, ReleaseSafe, ReleaseFast, and ReleaseSmall. Here we do not
    // set a preferred release mode, allowing the user to decide how to optimize.
    const optimize = b.standardOptimizeOption(.{});

    // Expose the libraries as modules.
    _ = b.addModule("multibase", .{ .root_source_file = .{ .path = "src/multibase.zig" } });
    _ = b.addModule("multihash", .{ .root_source_file = .{ .path = "src/multihash.zig" } });

    // Creates a step for unit testing. This only builds the test executable
    // but does not run it.
    const main_tests = b.addTest(.{
        .root_source_file = .{ .path = "src/main.zig" },
        .target = target,
        .optimize = optimize,
    });

    const coverage = b.addSystemCommand(&.{
        "kcov",
        b.fmt("--include-pattern={s}", .{b.pathFromRoot("src")}),
        b.pathFromRoot("kcov-out"),
    });
    coverage.addFileArg(main_tests.getEmittedBin());
    const coverage_run = b.step("coverage", "Run unit tests while capturing coverage.");
    coverage_run.dependOn(&coverage.step);

    const test_step = b.step("test", "Run library tests");
    test_step.dependOn(&main_tests.step);

    const docs_step = b.step("docs", "Build and install documentation");
    docs_step.dependOn(&b.addInstallDirectory(.{
        .source_dir = main_tests.getEmittedDocs(),
        .install_dir = .prefix,
        .install_subdir = "doc/multiformats",
    }).step);
}
