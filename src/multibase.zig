// Copyright (c) 2023 Gregory Oakes
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the “Software”), to deal in the Software without restriction,
// including without limitation the rights to use, copy, modify, merge, publish, distribute,
// sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
// BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

const std = @import("std");

/// An enumeration of the encoding formats supported by this library.
pub const MultiBase = enum {
    identity,
    base16,
    base16upper,
    base64,
    base64pad,
    base64url,
    base64urlpad,
};

const prefixes = blk: {
    var map = std.EnumMap(MultiBase, []const u8){};
    map.put(.identity, "0x00");
    map.put(.base16, "f");
    map.put(.base16upper, "F");
    map.put(.base64, "m");
    map.put(.base64pad, "M");
    map.put(.base64url, "u");
    map.put(.base64urlpad, "U");
    break :blk map;
};

const alphabets = blk: {
    var map = std.EnumMap(MultiBase, []const u8){};
    map.put(.base16, "0123456789abcdef");
    map.put(.base16upper, "0123456789ABCDEF");
    map.put(.base64, "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/");
    map.put(.base64pad, "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/");
    map.put(.base64url, "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789-_");
    map.put(.base64urlpad, "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789-_");
    break :blk map;
};

const pad_chars = blk: {
    var map = std.EnumMap(MultiBase, u8){};
    map.put(.base64pad, std.base64.standard.pad_char.?);
    map.put(.base64urlpad, std.base64.url_safe.pad_char.?);
    break :blk map;
};

/// A buffer sufficiently large enough to store `len` bytes using any `MultiBase`.
pub fn MaxEncoded(comptime len: usize) type {
    var res: usize = 0;
    inline for (std.enums.values(MultiBase)) |base|
        res = @max(res, calcMaxEncodedSize(base, len));
    return [res]u8;
}

/// A buffer sufficiently large enough to store `len` bytes encoded as `base`.
pub fn Encoded(comptime base: MultiBase, comptime len: usize) type {
    return [calcMaxEncodedSize(base, len)]u8;
}

/// Return the maximum number of bytes required to encode `len` bytes as `base`.
///
/// For most bases, this is the exact number of bytes to which `len` bytes will encode. However,
/// there are some odd encoding schemes (base58btc) which encode into a variable size which has a
/// known maximum.
pub fn calcMaxEncodedSize(base: MultiBase, len: usize) usize {
    return prefixes.get(base).?.len + switch (base) {
        .identity => len,
        .base16, .base16upper => len * 2,
        .base64 => std.base64.standard_no_pad.Encoder.calcSize(len),
        .base64pad => std.base64.standard.Encoder.calcSize(len),
        .base64url => std.base64.url_safe_no_pad.Encoder.calcSize(len),
        .base64urlpad => std.base64.url_safe.Encoder.calcSize(len),
    };
}

/// Encode `src` as `base` into `dest`, returning the written bytes slice.
///
/// The returned slice is a sub-slice of `dest` as thus has the same lifetime as `dest`.
///
/// Asserts `dest` is sufficiently large to encode `src`.
pub fn encode(base: MultiBase, dest: []u8, src: []const u8) []u8 {
    std.debug.assert(dest.len >= calcMaxEncodedSize(base, src.len));
    return switch (base) {
        .identity => encodeIdentity(dest, src),
        .base16 => encodeBase16(.lower, dest, src),
        .base16upper => encodeBase16(.upper, dest, src),
        .base64 => encodeBase64(.standard, dest, src),
        .base64pad => encodeBase64(.standard_pad, dest, src),
        .base64url => encodeBase64(.url, dest, src),
        .base64urlpad => encodeBase64(.url_pad, dest, src),
    };
}

/// Encode `src` into `dest` using the `.identity` encoding.
///
/// Asserts `dest` is sufficiently large to encode `src`.
pub fn encodeIdentity(dest: []u8, src: []const u8) []u8 {
    std.debug.assert(dest.len >= calcMaxEncodedSize(.identity, src.len));
    const prefix = comptime prefixes.get(.identity).?;
    @memcpy(dest[0..prefix.len], prefix);
    @memcpy(dest[prefix.len..][0..src.len], src);
    return dest[0 .. prefix.len + src.len];
}

/// Encode `src` into `dest` using either `.base16` or `.base16upper` encoding.
///
/// Asserts `dest` is sufficiently large to encode `src`.
pub fn encodeBase16(comptime case: std.fmt.Case, dest: []u8, src: []const u8) []u8 {
    std.debug.assert(dest.len >= calcMaxEncodedSize(.base16, src.len));
    const alphabet = comptime switch (case) {
        .lower => @as(@Vector(16, u8), "0123456789abcdef".*),
        .upper => @as(@Vector(16, u8), "0123456789ABCDEF".*),
    };
    const prefix = comptime prefixes.get(switch (case) {
        .lower => .base16,
        .upper => .base16upper,
    }).?;
    @memcpy(dest[0..prefix.len], prefix);
    const tail = dest[prefix.len..];
    var idx: usize = 0;
    // TODO: Is there any benefit to SIMD instructions? A naive reading of godbolt's output for this
    // function shows vector operations in the `-OReleaseFast` build even when no vector are used.
    // Does Zig automatically optimize them in when it is beneficial to use vector operations?
    //
    // Ref: https://zig.godbolt.org/z/cKzc79vd7
    // if (comptime std.simd.suggestVectorSize(u8)) |size| {
    //     const V = @Vector(size, u8);
    //     const lower_mask: V = @splat(0x0f);
    //     const upper_mask: V = @splat(0xf0);
    //     while (idx + size < src.len) : (idx += size) {
    //         const chunk: V = src[idx..][0..size].*;
    //         const lower = chunk & lower_mask;
    //         const upper = (chunk & upper_mask) >> @splat(4);
    //         @memcpy(tail[idx * 2 ..][0 .. size * 2], &@as([size * 2]u8, std.simd.interlace(.{
    //             // TODO: Why does this require the mask to be comptime?
    //             // @shuffle(u8, alphabet, undefined, upper),
    //             // @shuffle(u8, alphabet, undefined, lower),
    //             @select(
    //                 u8,
    //                 upper > @as(V, @splat(9)),
    //                 upper + @as(V, @splat(alphabet[10] - 10)),
    //                 upper + @as(V, @splat('0')),
    //             ),
    //             @select(
    //                 u8,
    //                 lower > @as(V, @splat(9)),
    //                 lower + @as(V, @splat(alphabet[10] - 10)),
    //                 lower + @as(V, @splat('0')),
    //             ),
    //         })));
    //     }
    // }
    while (idx < src.len) : (idx += 1) {
        const byte = src[idx];
        tail[idx * 2] = alphabet[(byte & 0xf0) >> 4];
        tail[idx * 2 + 1] = alphabet[byte & 0xf];
    }
    return dest[0 .. prefix.len + idx * 2];
}

/// Encode `src` into `dest` using the relevant base64 encoding variant.
///
/// Asserts `dest` is sufficiently large to encode `src`.
pub fn encodeBase64(
    comptime variant: enum { standard, standard_pad, url, url_pad },
    dest: []u8,
    src: []const u8,
) []u8 {
    const prefix = comptime prefixes.get(switch (variant) {
        .standard => .base64,
        .standard_pad => .base64pad,
        .url => .base64url,
        .url_pad => .base64urlpad,
    }).?;
    @memcpy(dest[0..prefix.len], prefix);
    const len = prefix.len + switch (variant) {
        .standard => std.base64.standard_no_pad.Encoder,
        .standard_pad => std.base64.standard.Encoder,
        .url => std.base64.url_safe_no_pad.Encoder,
        .url_pad => std.base64.url_safe.Encoder,
    }.encode(dest[prefix.len..], src).len;
    return dest[0..len];
}

/// Decode `src` as `base` into `dest`, returning the written bytes slice.
///
/// The returned slice is a sub-slice of `dest` as thus has the same lifetime as `dest`.
pub fn decode(dest: []u8, encoded: []const u8) ![]u8 {
    // Identify which enocding was used by looking for a matching prefix.
    const found = inline for (comptime std.enums.values(MultiBase)) |base| {
        const prefix = comptime prefixes.get(base).?;
        if (std.mem.startsWith(u8, encoded, prefix))
            break .{ base, prefix.len };
    } else return error.UnknownBase;
    const embedded = encoded[found[1]..];
    switch (found[0]) {
        .identity => {
            const dest_slice = dest[0..embedded.len];
            @memcpy(dest_slice, embedded);
            return dest_slice;
        },
        .base16, .base16upper => return try std.fmt.hexToBytes(dest, embedded),
        .base64 => {
            try std.base64.standard_no_pad.Decoder.decode(dest, embedded);
            return dest[0..try std.base64.standard_no_pad.Decoder.calcSizeForSlice(embedded)];
        },
        .base64pad => {
            try std.base64.standard.Decoder.decode(dest, embedded);
            return dest[0..try std.base64.standard.Decoder.calcSizeForSlice(embedded)];
        },
        .base64url => {
            try std.base64.url_safe_no_pad.Decoder.decode(dest, embedded);
            return dest[0..try std.base64.url_safe_no_pad.Decoder.calcSizeForSlice(embedded)];
        },
        .base64urlpad => {
            try std.base64.url_safe.Decoder.decode(dest, embedded);
            return dest[0..try std.base64.url_safe.Decoder.calcSizeForSlice(embedded)];
        },
    }
}

fn expectRoundTrip(comptime base: MultiBase, source: anytype, comptime encoded: []const u8) !void {
    var enc_buf: Encoded(base, source.len) = undefined;
    try std.testing.expectEqualStrings(encoded, encode(base, &enc_buf, source));
    var dec_buf: [source.len]u8 = undefined;
    try std.testing.expectEqualSlices(u8, source, try decode(&dec_buf, encoded));
}

// Credit: https://github.com/multiformats/multibase/blob/af2d36bdfaeaca453d20b18542ca57bd56b51f6c/tests/basic.csv
test "basic" {
    try expectRoundTrip(.identity, "yes mani !", "0x00yes mani !");
    try expectRoundTrip(.base16, "yes mani !", "f796573206d616e692021");
    try expectRoundTrip(.base16upper, "yes mani !", "F796573206D616E692021");
    try expectRoundTrip(.base64, "yes mani !", "meWVzIG1hbmkgIQ");
    try expectRoundTrip(.base64pad, "yes mani !", "MeWVzIG1hbmkgIQ==");
    try expectRoundTrip(.base64url, "yes mani !", "ueWVzIG1hbmkgIQ");
    try expectRoundTrip(.base64urlpad, "yes mani !", "UeWVzIG1hbmkgIQ==");
}

// Credit: https://github.com/multiformats/multibase/blob/af2d36bdfaeaca453d20b18542ca57bd56b51f6c/tests/leading_zero.csv
test "leading_zero" {
    try expectRoundTrip(.identity, "\x00yes mani !", "0x00\x00yes mani !");
    try expectRoundTrip(.base16, "\x00yes mani !", "f00796573206d616e692021");
    try expectRoundTrip(.base16upper, "\x00yes mani !", "F00796573206D616E692021");
    try expectRoundTrip(.base64, "\x00yes mani !", "mAHllcyBtYW5pICE");
    try expectRoundTrip(.base64pad, "\x00yes mani !", "MAHllcyBtYW5pICE=");
    try expectRoundTrip(.base64url, "\x00yes mani !", "uAHllcyBtYW5pICE");
    try expectRoundTrip(.base64urlpad, "\x00yes mani !", "UAHllcyBtYW5pICE=");
}

test "error.UnknownBase" {
    var buf: [64]u8 = undefined;
    try std.testing.expectError(error.UnknownBase, decode(&buf, "a"));
}
