// Copyright (c) 2023 Gregory Oakes
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the “Software”), to deal in the Software without restriction,
// including without limitation the rights to use, copy, modify, merge, publish, distribute,
// sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
// BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

const std = @import("std");
const Sha1 = std.crypto.hash.Sha1;
const sha2 = std.crypto.hash.sha2;
const sha3 = std.crypto.hash.sha3;

/// The default enumeration of possible codecs as defined in
/// https://github.com/multiformats/multicodec/raw/master/table.csv.
///
/// Only codecs which are marked as `permanent` are defined.
///
/// NOTE: The multiformat's specifications use varints with a max byte representation of 9. That's
/// why this enum has an unusual tag integer.
pub const MultiCodec = enum(u63) {
    const Self = @This();

    sha1 = 0x11,
    @"sha2-256" = 0x12,
    @"sha2-512" = 0x13,
    @"sha3-512" = 0x14,
    @"sha3-384" = 0x15,
    @"sha3-256" = 0x16,
    @"sha3-224" = 0x17,
    @"sha2-384" = 0x20,
    @"sha2-224" = 0x1013,
};

/// The maximum length of an encoded multihash created by this module.
pub const max_digest_length = blk: {
    var max: usize = 0;
    for (std.enums.values(MultiCodec)) |codec| {
        max = @max(max, Hasher(codec).digest_length);
    }
    break :blk max;
};

/// The largest encoded multihash created by this module.
pub const MaxDigest = [max_digest_length]u8;

/// Hash `input` encoding the multihash digest into `dest` and returning the written slice.
pub fn create(codec: MultiCodec, dest: *MaxDigest, input: []const u8) []u8 {
    var hasher = AnyHasher.init(codec);
    hasher.update(input);
    return hasher.final(dest);
}

/// A union of all `Hasher` types with a runtime known codec.
pub const AnyHasher = struct {
    pub const VTable = struct {
        update: *const fn (*anyopaque, []const u8) void,
        final: *const fn (*anyopaque, *MaxDigest) []u8,
    };
    const state_size = blk: {
        var res: comptime_int = 0;
        for (std.enums.values(MultiCodec)) |codec| {
            res = @max(res, @sizeOf(Hasher(codec)));
        }
        break :blk res;
    };
    const state_align = blk: {
        var res: comptime_int = 1;
        for (std.enums.values(MultiCodec)) |codec| {
            res = @max(res, @alignOf(Hasher(codec)));
        }
        break :blk res;
    };

    /// A union of all the hasher types as bytes.
    state_bytes: [state_size]u8 align(state_align),
    /// The vtable for the initialized codec.
    vtable: *const VTable,

    pub fn init(codec: MultiCodec) AnyHasher {
        switch (codec) {
            inline else => |ct_codec| {
                return .{
                    .state_bytes = std.mem.toBytes(Hasher(ct_codec).init(.{})) ++
                        [_]u8{undefined} ** (state_size - @sizeOf(Hasher(ct_codec))),
                    .vtable = &Hasher(ct_codec).vtable,
                };
            },
        }
    }

    pub fn update(self: *AnyHasher, bytes: []const u8) void {
        self.vtable.update(self.state(), bytes);
    }

    pub fn final(self: *AnyHasher, dest: *MaxDigest) []u8 {
        return self.vtable.final(self.state(), dest);
    }

    /// Calculate the pointer to the initialized codec's state.
    inline fn state(self: *AnyHasher) *anyopaque {
        return @ptrCast(&self.state_bytes);
    }

    pub const Writer = std.io.Writer(*AnyHasher, error{}, write);

    pub fn writer(self: *AnyHasher) Writer {
        return .{ .context = self };
    }

    fn write(self: *AnyHasher, bytes: []const u8) !usize {
        self.update(bytes);
        return bytes.len;
    }
};

/// Interface to incrementally create multihashes.
pub fn Hasher(comptime codec: MultiCodec) type {
    return struct {
        const Self = @This();

        const Inner = switch (codec) {
            .sha1 => Sha1,
            .@"sha2-256" => sha2.Sha256,
            .@"sha2-512" => sha2.Sha512,
            .@"sha3-512" => sha3.Sha3_512,
            .@"sha3-384" => sha3.Sha3_384,
            .@"sha3-256" => sha3.Sha3_256,
            .@"sha3-224" => sha3.Sha3_224,
            .@"sha2-384" => sha2.Sha384,
            .@"sha2-224" => sha2.Sha224,
        };

        const prefix = varint(@intFromEnum(codec)) ++ varint(Inner.digest_length);
        pub const digest_length = prefix.len + Inner.digest_length;
        pub const Digest = [digest_length]u8;

        inner: Inner,

        pub fn init(opts: Inner.Options) Self {
            return .{ .inner = Inner.init(opts) };
        }

        pub fn update(self: *Self, bytes: []const u8) void {
            self.inner.update(bytes);
        }

        pub fn final(self: *Self, dest: *Digest) void {
            @memcpy(dest[0..prefix.len], prefix);
            self.inner.final(dest[prefix.len..][0..Inner.digest_length]);
        }

        const vtable = AnyHasher.VTable{
            .update = @ptrCast(&update),
            .final = @ptrCast(&finalErased),
        };

        fn finalErased(self: *Self, dest: *MaxDigest) []u8 {
            self.final(@ptrCast(dest));
            return dest[0..digest_length];
        }
    };
}

/// Comptime encode an integer into a varint byte slice.
fn varint(comptime int: u63) []const u8 {
    return comptime blk: {
        var res: []const u8 = &[_]u8{};
        const chunk: u8 = std.math.maxInt(u7);
        var rem = int;
        while (true) {
            const more = rem > chunk;
            var byte = @as(u8, @truncate(rem & chunk));
            if (more) {
                byte |= 1 << 7;
            }
            res = res ++ &[_]u8{byte};
            if (!more) break;
            rem >>= 7;
        }
        break :blk res;
    };
}

test "encode varint" {
    try std.testing.expectEqualSlices(u8, "\x00", varint(0));
    try std.testing.expectEqualSlices(u8, "\x01", varint(1));
    try std.testing.expectEqualSlices(u8, "\x80\x01", varint(128));
    try std.testing.expectEqualSlices(u8, "\xff\x01", varint(255));
    try std.testing.expectEqualSlices(u8, "\xac\x02", varint(300));
    try std.testing.expectEqualSlices(u8, "\x80\x80\x01", varint(16384));
    inline for (.{ u7, u14, u21, u28, u35, u42, u49, u56, u63 }, 0..) |T, idx| {
        try std.testing.expectEqualSlices(u8, "\xff" ** idx ++ "\x7f", varint(std.math.maxInt(T)));
    }
    inline for (.{ u7, u14, u21, u28, u35, u42, u49, u56 }, 0..) |T, idx| {
        try std.testing.expectEqualSlices(
            u8,
            "\x80" ** (idx + 1) ++ "\x01",
            varint(std.math.maxInt(T) + 1),
        );
    }
}

/// Expect a hash of type `codec` over `input` to produce the encoded multihash `expected` when
/// truncated to `bits`.
///
/// WARNING: This is meant for testing only. DO NOT use to verify hashes.
fn expectHash(codec: MultiCodec, input: []const u8, expected: []const u8) !void {
    var buf: MaxDigest = undefined;
    const actual = create(codec, &buf, input);
    try std.testing.expectEqualSlices(u8, expected, actual);
}

test "sha1" {
    try expectHash(.sha1, "4c98758072933abd55fef35782cf37386db7ce4dce", "\x11\x14\x00\x88\xdb\xe5\xcf\xca\xc4\xd7\x8f\x2a\x6b\x9f\x00\x78\x00\x7b\x31\x41\xe7\xfc");
    try expectHash(.sha1, "8a461139f4d5078f6ff77af4fbc1befc775c0e0cb0", "\x11\x14\x09\x87\xa9\x72\xa0\x12\x2c\x7f\x07\x88\xfe\x35\xb3\x50\x5a\xdb\x2d\xa8\x89\xe7");
    try expectHash(.sha1, "29a53e335621740c0fbc8dd85970fa0311b0d58e25", "\x11\x14\x22\xb6\xcc\x60\xa0\x78\x2c\x14\x5d\xc9\x25\xd2\xc3\xd0\x2e\x72\x2d\x56\x4f\xe9");
    try expectHash(.sha1, "557f47c855a5ca40daa3c0904a1e43647b4021ce0c", "\x11\x14\x40\xc9\x5e\xd8\x9d\x5a\x64\x8d\x40\x57\xe6\xe5\x7d\x79\x06\xdd\x58\xd2\xbb\x66");
    try expectHash(.sha1, "a244e61f124cd3590173e2809b1177ca611ff15e9a", "\x11\x14\x42\x0c\xe8\x5f\xe6\x64\x8e\x36\x71\x14\xf0\x73\xae\x09\x58\x1d\x2d\x5c\x92\xda");
    try expectHash(.sha1, "eaf89d6d7fa2ee40c7357e3d627cbd1338009570f5", "\x11\x14\x69\xa4\x9a\xbe\xa4\x0f\x7b\x33\x51\x5d\x2c\x78\xac\x3a\xd2\x7c\x96\x59\xdd\xf8");
    try expectHash(.sha1, "8fc7992293335bb2e2f6d962f66734a652e495022e", "\x11\x14\x6c\xc3\x9b\x02\xd7\x28\x94\x54\x59\xe0\x8e\x4a\x63\x8b\x1d\x1b\xc3\x64\x8e\x6e");
    try expectHash(.sha1, "79f28aba59f256868f8b7f868e75aedcab8b068be0", "\x11\x14\x81\xaa\xff\x20\x8d\x63\x69\x1d\x51\x76\xd4\xaa\xf2\x7a\xa5\x95\x59\x38\x58\x87");
    try expectHash(.sha1, "f7f96cf1e683e6f7e752dab4613a1b1225889bb15d", "\x11\x14\x92\x09\x4d\xd3\xee\xd1\x99\x7c\xfa\x0d\x4c\x97\x43\xc6\xc0\x3d\x9a\xb9\xfe\xe5");
    try expectHash(.sha1, "7f0bb0ac4dd2404c097dade4c8b2292ae2419b7a70", "\x11\x14\x92\xec\xbf\x3f\xac\xfa\x79\x11\x8d\x36\x11\xe7\xe4\x06\xf4\xd4\xd6\x24\x2f\x1c");
    try expectHash(.sha1, "4c44254356730838195a32cbb1b8be3bed4c6c05c0", "\x11\x14\x96\xc5\x11\x4c\x52\xa3\xea\x78\x73\x60\x3e\xb5\x9d\xa1\xab\x88\xc0\xec\x2c\xeb");
    try expectHash(.sha1, "fe2bd8cb6ad52767eae6fbd354ceb3e72e8f55669b", "\x11\x14\x9d\xae\xb5\xb0\xeb\x9d\x59\x50\x16\x35\xb2\x0b\x1f\x1c\x2e\x12\xe4\x81\xad\xae");
    try expectHash(.sha1, "2d6db2d7882fa8b7d56e74b8e24036deb475de8c94", "\x11\x14\xa2\x9e\xea\x78\x88\x60\xfb\x1b\x28\x84\x08\x34\x55\x92\xc8\x66\x2a\x4d\xcd\xe5");
    try expectHash(.sha1, "1391551dc8f15110d256c493ed485bca8cfed07241", "\x11\x14\xae\x13\x51\x50\x6b\x46\x4f\x8e\x5d\xeb\x0f\x6a\x2b\x2a\x27\x2d\xca\xcb\xfa\xdf");
    try expectHash(.sha1, "444a8ca03cce5b47991b3baf807f4eaa1d8175a72e", "\x11\x14\xbd\x23\x22\x0d\xec\x58\x3a\xdd\xf2\x75\x99\xf0\xdc\x3a\x53\xc2\xa1\xdc\xf8\x06");
    try expectHash(.sha1, "f6896cafdd752d9615f22017dfce1a281fb0598038", "\x11\x14\xc8\x0f\xba\x19\x87\x56\x7e\xa9\xf3\xff\x4a\x58\x7d\xa9\xa5\x37\xa3\x65\x5f\x46");
    try expectHash(.sha1, "6c00022ba29d15926c4580332ded091e666f0ec5d9", "\x11\x14\xc8\xa5\xb4\x45\xce\x47\x0e\x4b\xc5\x48\x84\xe6\x0e\x48\xe3\x1f\xe1\x2c\xb1\xa6");
    try expectHash(.sha1, "431fb5d4c9b735ba1a34d0df045118806ae2336f2c", "\x11\x14\xe8\x61\xe4\x52\xcf\xd8\x4d\xca\x9a\x17\x62\x58\xc3\xd0\x6e\x02\x0a\x0c\x93\xd8");
    try expectHash(.sha1, "b07cb1832e7e253f3eb7cf4029a82d881217d3c02f", "\x11\x14\xf7\x16\x5b\x40\x91\x3f\xe4\x11\xae\x00\x12\x43\x5a\xc0\xac\x94\x91\xcd\x68\xe9");
    try expectHash(.sha1, "3647da9e56b1afffa9921ce9a9caaa51ac8a166706", "\x11\x14\xfb\xce\x59\xad\x61\xc0\xec\x28\xed\x14\x40\x6c\x14\x32\xc9\x79\x3b\x57\xad\xec");
}

test "sha2-256" {
    try expectHash(.@"sha2-256", "a244e61f124cd3590173e2809b1177ca611ff15e9a", "\x12\x20\x2e\xfc\xc6\x5f\xc3\x70\x70\xbb\x59\x73\xdf\x7a\x99\xbb\xb2\xbc\x19\xa7\xdf\xe0\x5c\x44\x8d\xd2\xe0\x52\xac\xe4\x6d\xf9\xa8\x0b");
    try expectHash(.@"sha2-256", "2d6db2d7882fa8b7d56e74b8e24036deb475de8c94", "\x12\x20\x3a\xfa\x34\xfb\xa2\xf3\x57\x2a\xc5\x6d\x80\xf5\x20\x02\xe8\x72\x7d\xd9\x5f\x17\xa2\xe7\xc9\x37\x6a\x31\x47\xa9\x6d\x3e\x78\xac");
    try expectHash(.@"sha2-256", "f7f96cf1e683e6f7e752dab4613a1b1225889bb15d", "\x12\x20\x43\x41\xe7\x83\x2a\x06\x25\xd5\x78\x45\x1d\xf1\xd6\xf0\x7b\x36\xfd\xec\x45\x05\x48\x4e\xde\x11\x61\x58\x93\xa5\xd1\x16\x2b\xdc");
    try expectHash(.@"sha2-256", "f6896cafdd752d9615f22017dfce1a281fb0598038", "\x12\x20\x45\x77\xee\x99\xca\x45\xb0\x0b\xae\x3a\xfe\x7f\x3a\x50\xcb\xaf\xb1\x09\xef\x1a\x16\x5e\xc6\x98\xcb\x29\xa6\x2d\xaf\x1a\xdd\xab");
    try expectHash(.@"sha2-256", "29a53e335621740c0fbc8dd85970fa0311b0d58e25", "\x12\x20\x66\xb0\xaa\x77\x2b\x75\x62\xdb\xf5\x34\xae\x65\x3a\xb6\x86\x73\x5b\xaf\x7e\xd3\x61\x4c\xaa\xd3\x98\x86\x8d\x8e\x2b\x84\xa2\x89");
    try expectHash(.@"sha2-256", "b07cb1832e7e253f3eb7cf4029a82d881217d3c02f", "\x12\x20\x88\xaf\x10\x51\x16\xb3\x00\x6c\x14\xcc\x9c\x38\xe5\xbd\x90\x49\x38\x6d\xc8\x27\x85\xfb\xb1\x5a\x69\xa9\x07\xfb\x9d\xa8\xc8\x31");
    try expectHash(.@"sha2-256", "4c98758072933abd55fef35782cf37386db7ce4dce", "\x12\x20\x89\x88\xc7\x16\x53\x61\x84\x42\x6e\x96\xe1\x71\x49\x64\xc1\x8a\x3e\xc7\xc3\xdf\x53\x7d\x69\x2b\x41\x07\x28\xbf\x79\xe1\x68\x68");
    try expectHash(.@"sha2-256", "1391551dc8f15110d256c493ed485bca8cfed07241", "\x12\x20\x8a\x54\xb2\xb6\xb0\x31\xda\x07\xe7\x20\x55\x82\x8c\xbf\x41\xad\x25\x62\xa6\xf3\x67\x5f\xcc\x84\x85\x75\x77\xcb\xd1\xa9\x35\xc1");
    try expectHash(.@"sha2-256", "fe2bd8cb6ad52767eae6fbd354ceb3e72e8f55669b", "\x12\x20\x9e\xea\xb6\x80\xac\x6b\xd5\x43\xdf\xc3\x4b\x3d\x90\xaa\x6e\x5a\x4b\xb2\x79\x61\xfa\xef\x39\x68\x0e\x3f\x54\x95\x16\xdc\x94\x8e");
    try expectHash(.@"sha2-256", "557f47c855a5ca40daa3c0904a1e43647b4021ce0c", "\x12\x20\xb1\x7e\x5d\x52\xe9\x4b\x25\xc5\xfe\x09\x8c\x23\x31\x33\xc5\xd4\x35\x56\xb3\x5d\x2a\x50\xc5\x9c\xf6\x17\xfc\x61\xdf\x17\x13\xfd");
    try expectHash(.@"sha2-256", "4c44254356730838195a32cbb1b8be3bed4c6c05c0", "\x12\x20\xb8\x01\x8a\x7f\x12\xb6\xda\x38\x92\x60\x81\x67\xfc\x5e\xad\x90\xfc\x57\xfd\x1e\x83\xae\xfe\x94\x53\xb6\x23\x02\x2d\x5f\x2b\x9b");
    try expectHash(.@"sha2-256", "8fc7992293335bb2e2f6d962f66734a652e495022e", "\x12\x20\xc9\x4a\xb4\x83\x0a\x2f\x1e\xef\x4e\x37\x8a\x49\x9d\x91\xf0\x9f\x1a\x90\x9a\x24\x9c\x48\xb2\x7d\x33\x66\xee\x6d\x97\xc2\xfc\x2e");
    try expectHash(.@"sha2-256", "eaf89d6d7fa2ee40c7357e3d627cbd1338009570f5", "\x12\x20\xcf\x99\x91\xb4\x0b\x27\x3a\xf2\xef\x5c\x3d\x41\x66\x85\x39\xd7\xad\xfc\x26\x37\xda\xd7\x10\x25\x93\xea\x36\xb2\x00\x35\x51\x24");
    try expectHash(.@"sha2-256", "8a461139f4d5078f6ff77af4fbc1befc775c0e0cb0", "\x12\x20\xdd\xc3\xfc\xda\x5d\x7f\xc1\xf3\xc4\x8a\x28\x4a\xca\x1b\x54\x40\xb5\x4e\xe8\xdd\x1e\x2c\x4d\xbb\x7b\xcb\x77\xdb\x69\x6f\x71\xe2");
    try expectHash(.@"sha2-256", "3647da9e56b1afffa9921ce9a9caaa51ac8a166706", "\x12\x20\xe9\x6d\x03\x71\x44\x2d\x2a\x8e\xae\x11\x9e\xef\xe1\xd8\x71\x0e\x8c\x59\x56\x2a\x8f\xe7\x0a\xdc\xba\x25\x32\x2d\xe2\xa0\xf8\x54");
    try expectHash(.@"sha2-256", "6c00022ba29d15926c4580332ded091e666f0ec5d9", "\x12\x20\xef\x38\x7c\x6f\xea\x7a\xb7\x02\xd4\x70\xa9\x43\x2c\x4c\xde\x44\x19\x9c\x8d\x06\xc6\xea\xa5\xa1\xf2\xc6\x1c\xfb\x0b\x70\xa9\x7f");
    try expectHash(.@"sha2-256", "7f0bb0ac4dd2404c097dade4c8b2292ae2419b7a70", "\x12\x20\xf0\x00\x49\x98\x60\xab\xad\xcf\x61\xed\xec\x74\x53\xc0\xa9\x11\x2c\x50\x83\xed\x9f\xa2\x7c\x35\x0d\xed\x0a\x8e\xd2\x7e\xa2\x36");
    try expectHash(.@"sha2-256", "79f28aba59f256868f8b7f868e75aedcab8b068be0", "\x12\x20\xf2\x8c\xd1\x1d\xcb\x49\xa1\x31\xf9\xb8\x77\x30\x35\xbd\xc2\x60\x5a\x6c\x2d\x64\x59\x78\x2b\x98\x7d\x08\xc5\xa8\xc1\xfd\x18\x14");
    try expectHash(.@"sha2-256", "444a8ca03cce5b47991b3baf807f4eaa1d8175a72e", "\x12\x20\xf6\xa1\x70\x55\xda\x69\x21\xb9\x2a\x23\xf6\xfa\x4e\x24\x34\x16\x0d\xec\x6c\xcf\xe1\xce\x59\x79\x41\x5c\xcf\x64\x67\xa8\xd8\xac");
    try expectHash(.@"sha2-256", "431fb5d4c9b735ba1a34d0df045118806ae2336f2c", "\x12\x20\xff\xb3\x1f\x07\xaa\x15\x34\x83\x68\xc9\x0c\x73\xa8\x34\xdb\xf9\xf8\x71\x03\x65\x86\x0f\xa0\xfd\x4d\xdc\xe9\xac\x27\x82\xcc\x17");
}

test "sha2-512" {
    try expectHash(.@"sha2-512", "557f47c855a5ca40daa3c0904a1e43647b4021ce0c", "\x13\x40\x03\x39\x34\x73\x18\xe4\x0b\xf2\x3f\xc0\x3c\x93\x56\x9d\xf1\x9c\x76\x7e\x24\x95\xcd\xcf\xa3\x0a\xa7\x38\x5f\xd4\x3d\xd6\xa9\x1f\xdd\x0c\x39\x62\xbe\x32\x4c\xf4\xc2\x52\x7c\xc1\x7a\xa6\x5a\xa0\x34\x9d\xf6\x26\x05\x99\xe3\x8c\x7b\xd2\xfe\xa9\xa1\x78\xa6\x6b");
    try expectHash(.@"sha2-512", "2d6db2d7882fa8b7d56e74b8e24036deb475de8c94", "\x13\x40\x1c\x45\x2c\x7d\x9f\x57\x25\x10\xfa\x1a\x32\x24\xe3\x7a\x30\x30\xc8\x75\x0b\x3e\x17\x63\xf1\xad\x0d\x38\x27\x4b\x3e\xa5\x8c\xa7\xb4\x31\x7d\xd0\x91\x32\x50\xb4\xb1\x6c\x9b\x0f\x62\x81\xaa\x74\xc4\x2d\x8d\x5e\x72\x04\x3e\x0f\xb2\x14\xd3\xad\x51\xd4\xee\x8c");
    try expectHash(.@"sha2-512", "8a461139f4d5078f6ff77af4fbc1befc775c0e0cb0", "\x13\x40\x3d\xeb\x07\xd2\xb0\xfe\x98\x91\x14\x9e\x56\x81\x62\x5b\xa7\x8c\x2b\x30\x77\x7c\xff\xfb\x54\x3d\x56\x72\xf8\x3a\xa2\xb6\xa0\x3e\x61\xc7\x27\x38\xec\x3d\x1e\xb6\x1d\xf1\xd1\x69\x1d\xa9\xfe\x51\x6b\x8f\x96\x68\xe1\x4a\x38\xc0\xb6\x20\xd1\xd7\xea\xc0\xe3\xbb");
    try expectHash(.@"sha2-512", "fe2bd8cb6ad52767eae6fbd354ceb3e72e8f55669b", "\x13\x40\x62\x1c\xb2\x04\x00\x60\xc6\x02\x4e\x57\xe9\xd6\x8a\xbb\x8c\x41\xec\x55\xb3\x27\xb9\xf2\xe5\x86\x5e\x65\xa1\xff\x86\x09\xd0\x0f\xb2\x7c\x41\x9e\xc9\xa6\x89\x78\xb8\x36\x52\xa4\x4f\x47\xd7\x86\x6b\xa5\x77\x17\xa0\x20\x3b\xa1\x17\xeb\x01\xcf\x44\x9c\x1e\xc7");
    try expectHash(.@"sha2-512", "a244e61f124cd3590173e2809b1177ca611ff15e9a", "\x13\x40\x62\x76\xd2\x5a\x66\xa2\x73\x1c\x3c\x2e\xb1\xde\xf1\xb0\xcd\xd0\x77\xf2\xd8\x2d\x03\x18\xe4\x37\x46\xc9\x28\xe4\x9c\xa2\x02\x6a\x5f\x8e\x11\x83\x93\xf4\x69\x93\xe6\xe9\x59\x21\x23\x18\xa4\xc6\xfa\xe8\xe6\x6f\x84\x30\xb7\x00\x09\x7c\xd6\x19\x2e\x8c\x35\x35");
    try expectHash(.@"sha2-512", "4c44254356730838195a32cbb1b8be3bed4c6c05c0", "\x13\x40\x68\x85\xc2\xf6\x12\x7b\xc4\x32\xff\x56\x19\x88\x55\xee\x65\x43\xa8\x21\xc4\xb6\xce\xd4\xe2\x50\xd1\xb5\xc9\xd6\x28\xe2\x4b\x94\x6a\x0a\xa1\xe5\xeb\xa5\x43\x10\xd8\x71\x0e\x26\x07\x5c\x82\xad\x0b\xa6\xa8\xf8\x63\x9d\xd5\x63\x66\x97\x6b\x79\xd0\x67\xf3\x68");
    try expectHash(.@"sha2-512", "f7f96cf1e683e6f7e752dab4613a1b1225889bb15d", "\x13\x40\x6f\x55\x83\x62\xcb\x0c\x86\x7f\x40\xbd\x8f\xda\x18\xd0\x04\xa1\x42\x06\xd0\x24\x38\x79\xc1\xff\x80\xdc\x70\xc8\x9d\x60\xea\xf8\xed\x19\xa9\x6c\x6c\x5a\x0d\x38\x59\x7a\x02\x7a\x30\x85\x6a\x6b\xe4\xc2\x5e\x5c\xe8\x47\x3f\xa9\xeb\xd6\xf8\x34\xc4\xbb\xda\xb3");
    try expectHash(.@"sha2-512", "8fc7992293335bb2e2f6d962f66734a652e495022e", "\x13\x40\x76\x39\x93\xfe\xbe\xab\x71\x4f\x75\xca\x06\x51\x1c\x52\xdd\xb6\x9d\xa2\xfa\xaf\x66\x54\xa1\xe9\xa9\xdc\x54\x5c\x63\x9e\xe2\xc9\x64\xde\xdd\xf5\xb0\xa1\xaa\xae\x5b\x32\xd2\xc4\x45\xb3\x72\x78\xa1\xba\x1d\xe3\x94\x80\x33\x01\x2d\xbc\x42\x78\xdb\xba\x0e\xa0");
    try expectHash(.@"sha2-512", "b07cb1832e7e253f3eb7cf4029a82d881217d3c02f", "\x13\x40\x7d\x18\x5f\x12\xce\x40\x0c\xe8\xd3\xe5\x5e\xf3\xa1\x26\xe8\xf6\xed\x89\xdd\x5d\xa8\xe2\x96\xe8\x4b\x7d\xa1\x05\xbf\x28\xd6\x43\x7e\xec\x01\x3a\x5e\x76\x00\x65\x22\x17\x91\xea\x7a\x2d\x4a\x84\x58\xd7\xd4\x54\xb9\xac\xbe\x74\x18\xdb\x05\xdc\xe0\xb5\x41\x2c");
    try expectHash(.@"sha2-512", "29a53e335621740c0fbc8dd85970fa0311b0d58e25", "\x13\x40\x8c\xad\x36\xff\x75\xb5\x98\x96\x8e\x94\xc5\x15\x28\x00\xfe\x2a\x9d\x6d\xe6\xcc\xa9\x26\x17\xc0\xfb\x33\x1b\xf9\x53\x9a\x80\xf7\xb5\x5d\xe9\x8f\xda\x87\x08\xdd\x77\x1f\xb3\x59\xef\x72\xce\x37\x94\x44\x0d\x10\x4e\xf7\xbe\xf5\x01\xdd\xb0\x17\x35\xd2\x85\x5f");
    try expectHash(.@"sha2-512", "6c00022ba29d15926c4580332ded091e666f0ec5d9", "\x13\x40\xa1\xe9\xa6\x3a\x38\x35\xd6\x99\x83\xdd\xde\xfe\x78\xf1\xd9\xb2\x33\xc9\x84\x4d\x42\xac\x14\xbb\x4e\x62\xd3\x9f\x1f\x00\x7b\x35\x0d\xc1\x52\xa0\xb3\xf3\xcb\xd2\x0b\xfd\xf0\xe1\x9d\x9b\x62\xca\x71\xf1\xae\x8a\x8c\x7e\x67\x26\x8c\xa9\x23\x25\xd5\x49\xde\xb3");
    try expectHash(.@"sha2-512", "431fb5d4c9b735ba1a34d0df045118806ae2336f2c", "\x13\x40\xab\xff\xa1\x92\x6b\x03\x8a\x2f\x18\x33\x09\x2d\x93\x85\x9b\xa2\xee\x56\xe0\x7a\x86\xb1\x58\x20\x0f\xc7\xd4\xd5\xd1\xea\xf3\x50\xb0\xf0\xe5\x11\xea\x6f\x50\x43\xa9\xf4\xa7\xc7\x88\x6f\x93\x35\x56\x52\x11\x47\x09\xd7\x5b\x2d\xb4\xc3\x37\x41\x12\x4f\xd4\xfe");
    try expectHash(.@"sha2-512", "444a8ca03cce5b47991b3baf807f4eaa1d8175a72e", "\x13\x40\xae\xea\xaf\x6c\xbf\xe3\xb6\x1b\x01\xf4\xda\xbe\xe8\xca\xd7\xc3\x06\x3b\x3a\xe8\x0e\x0f\x34\x6c\x52\x12\x69\x9c\x5b\xf3\x69\xfb\x57\x14\x3a\x7b\x4a\x98\x0f\xe5\xbf\xdf\x04\x20\xda\x7f\xf2\x66\x75\x30\xa1\x6f\x2c\x2d\x96\xc7\xc5\xc8\x58\x58\x4b\x15\x1b\x49");
    try expectHash(.@"sha2-512", "79f28aba59f256868f8b7f868e75aedcab8b068be0", "\x13\x40\xb1\x84\xc8\xff\xda\x37\x91\xa2\x91\x6a\x63\x2e\x9f\x59\xaa\xf1\x3e\x76\x26\x67\xd5\xcd\x8f\x51\x4c\xf3\x8d\x90\x93\x3a\x6c\x37\x34\xfd\x4a\xb3\xb0\xb3\xe5\xa9\xa3\x70\x1e\xb7\x61\xbe\xca\x3e\x01\xa5\xef\xcc\x25\x58\x07\x7f\x7d\x30\x1f\x30\x92\xe8\xab\xec");
    try expectHash(.@"sha2-512", "7f0bb0ac4dd2404c097dade4c8b2292ae2419b7a70", "\x13\x40\xbf\x1a\xcd\x49\xf8\x9a\x81\xfa\x60\x99\x5a\x03\x70\x9a\xe4\x83\x85\x0a\xbb\x0e\x1f\x16\x8e\x75\xa0\x2d\xb9\xd0\xb9\xad\x58\xc0\x05\x32\x02\xcf\x4d\xff\x68\x11\x28\x35\x19\x1b\x07\x06\x76\x93\xd6\x5d\x73\x72\x7d\xb8\x1a\x06\x24\xcc\xff\xdd\x92\xa8\x7f\xa2");
    try expectHash(.@"sha2-512", "f6896cafdd752d9615f22017dfce1a281fb0598038", "\x13\x40\xc0\x39\x89\x9e\xef\x73\x8c\x41\x7d\xcf\x0b\x59\x35\x8c\x4c\xf0\x27\x49\x64\x58\x93\x73\x4e\xda\x05\x87\x1f\x7c\xcb\xe8\x90\x64\xdb\xf9\x5c\x96\xed\xc6\x24\x5c\x54\xec\x56\x01\xf4\x4a\xfc\xea\x12\x6d\x0b\xa6\x6f\x0a\x54\x23\xab\x7a\xc2\xe9\x1e\x14\x48\x97");
    try expectHash(.@"sha2-512", "3647da9e56b1afffa9921ce9a9caaa51ac8a166706", "\x13\x40\xc4\x1a\x06\x3c\x61\x1e\x88\x27\x14\xd8\x42\x4f\x4e\xdd\x00\x09\xb4\xb7\xc5\x25\xf1\x32\x44\x6a\xf4\x57\x80\xbf\xe4\xff\x99\x80\x01\xc6\x47\x9f\x48\xae\x32\x89\xea\x40\xa8\x76\xa1\x6e\x33\xc9\xd6\xb9\x02\x01\xa3\x07\x54\x99\x38\xe3\x3b\xeb\x5e\x73\xe9\xf0");
    try expectHash(.@"sha2-512", "4c98758072933abd55fef35782cf37386db7ce4dce", "\x13\x40\xcf\x9b\x40\x08\xbf\xd4\xd6\x17\x03\x5c\x56\x86\xcc\x21\xbf\xb9\xc5\xd0\x99\x36\x85\x86\x89\x21\x75\x8a\x36\x9a\x24\x34\xf3\xb2\x6e\x01\x12\x4a\x0d\x42\x79\x7f\x0a\x30\xbf\xca\x8d\xb3\x24\x79\x8b\x85\x78\x16\xe7\xf4\x63\x7e\x55\xa4\x94\x1c\x59\x74\xc3\x51");
    try expectHash(.@"sha2-512", "eaf89d6d7fa2ee40c7357e3d627cbd1338009570f5", "\x13\x40\xd9\xbb\x89\x32\x6f\x0d\x56\xc5\xd5\xc5\xee\x71\xc1\xa0\xcf\x21\x38\xdb\x61\x46\x12\x6e\xa1\xf1\x35\xbb\xeb\xf1\x36\xd2\x9b\xf9\x65\x8b\xfc\x9a\xc3\xa5\xac\x09\x6c\x60\x84\x2f\x10\x08\xd6\x16\x2c\xef\xc4\xbb\xea\xbc\x41\x3f\x5e\x5d\x0c\xef\xf9\x4f\xa3\x44");
    try expectHash(.@"sha2-512", "1391551dc8f15110d256c493ed485bca8cfed07241", "\x13\x40\xeb\x89\xb3\xc6\x22\x5e\x2b\x0b\x98\x4e\x52\x67\x26\x07\x59\xba\x43\x35\x1a\x04\xd8\x76\x99\x73\xa2\xd5\x4b\x67\x0d\x96\xb9\x8e\x0d\x90\xcb\x7d\xdd\xcc\x37\x0c\x5b\x4f\x75\xbe\x26\x0f\x09\x27\xda\x42\x15\xb0\x95\x2a\x6c\xd4\xed\xef\xaf\x35\xf6\x88\x21\xd9");
}

test "sha3" {
    try expectHash(.@"sha3-512", "29a53e335621740c0fbc8dd85970fa0311b0d58e25", "\x14\x40\x28\xb6\x81\xdb\x6b\x56\x74\x38\x9e\x1b\xe7\xa4\x10\x88\x72\x60\xec\xc8\x16\x9e\xff\xa3\x42\x90\x7e\xe1\x29\x4d\x7a\x13\xcd\x20\x3e\xce\x58\x16\xb9\xbc\x38\x66\xe1\x0d\x1c\x9d\x5b\xe0\x0e\x4e\x6a\xd0\x54\x82\xe6\xb6\xb3\xe7\x6e\x6f\x8b\x66\x9a\x3a\xae\xf3");
    try expectHash(.@"sha3-512", "7f0bb0ac4dd2404c097dade4c8b2292ae2419b7a70", "\x14\x40\x39\xe3\xd6\x91\x86\xb5\x25\x2f\xd6\x9c\xff\x64\x6c\x2f\x12\xea\xa3\xf6\x37\x72\xe6\x06\x2a\x16\x57\xdc\x3a\xf9\x62\x3e\x2e\x57\xbf\x3c\x74\xb7\xd1\xf9\x6f\x4a\x4d\x0c\x8b\x85\xcd\x7b\x2b\xfd\xcb\x0f\x97\x12\x79\x57\x26\x37\x8d\x9c\xcc\xcd\x19\x2d\x80\x86");
    try expectHash(.@"sha3-512", "4c44254356730838195a32cbb1b8be3bed4c6c05c0", "\x14\x40\x3d\xc4\x3b\x47\x9f\x5e\x9e\x00\x8a\x52\x56\xca\x44\x2e\x66\x28\x69\x95\xc3\x9d\xeb\x73\x2a\x6f\xb4\xe2\xe7\x91\xa3\xc4\xa6\xa6\xe5\x32\x2e\x57\x1e\x94\x5a\x78\x74\x88\x96\xed\xc6\x78\x66\xab\x00\xc7\x67\x32\x0f\x69\x56\x83\x38\x57\xea\xb9\x91\xa7\x3a\x77");
    try expectHash(.@"sha3-512", "8fc7992293335bb2e2f6d962f66734a652e495022e", "\x14\x40\x45\x95\x06\xc8\xc2\x67\x89\xf2\x03\xac\xd8\x27\xc8\x4b\xf1\xc6\x8c\x82\x1f\x80\x0a\x1b\x97\x73\x06\xd5\x74\xd9\xc5\xfc\xc6\x37\xeb\x2a\x42\x55\x74\x2d\x3a\x6b\x24\x7b\xfc\x18\x89\x83\xa6\x33\xe5\x07\x4e\x98\xfc\xae\xe4\xc5\xf4\x3f\xe2\x11\xcc\x25\x68\x91");
    try expectHash(.@"sha3-512", "6c00022ba29d15926c4580332ded091e666f0ec5d9", "\x14\x40\x47\xf5\x7e\x20\x56\x01\x0a\xfa\x03\xbc\x58\x14\x1b\xa3\x75\x4f\x41\x91\x75\x18\xc8\x17\x11\x23\x6e\xac\xa3\x76\x6e\x33\x3b\x9a\x2a\x76\x7a\x90\x36\x3f\x7a\x17\x9e\x77\x6d\x85\xaa\x66\x10\x71\x37\x09\xee\x46\x53\x1f\x9a\x45\x45\x65\xf7\x37\xc6\x8b\xdc\x56");
    try expectHash(.@"sha3-512", "4c98758072933abd55fef35782cf37386db7ce4dce", "\x14\x40\x51\xe7\xbd\x1d\x17\x33\x16\x15\x6e\x0c\xfe\xb2\x81\x86\xda\x1a\x6d\xf4\xd5\x42\xad\x96\x8b\x1c\x03\x8a\x5f\x66\xc7\xee\xd6\x5a\xdd\x64\x65\xe7\x9d\x51\x89\x9d\xb0\x27\x8e\x11\xb3\xcb\xfa\x04\x82\x19\x67\xfe\xaa\x82\x2e\xea\x22\x94\x8b\x05\xc4\x64\xcf\x93");
    try expectHash(.@"sha3-512", "444a8ca03cce5b47991b3baf807f4eaa1d8175a72e", "\x14\x40\x55\x41\xd5\x14\x8a\x03\xe5\xba\x0a\x91\x6b\x8f\x91\x17\x52\x19\xb1\x5e\x26\x0f\x0b\x26\xc4\xfd\xfa\x24\xb2\x4b\xc5\x4a\xc6\x40\x45\xea\x81\x30\x27\x25\x2d\x05\xc4\x3a\x3f\x0d\x5d\xef\x49\x29\x37\xa3\x3f\xbc\x04\xc3\xea\xf7\xad\x61\xaf\x5e\x16\xbb\x7c\x3f");
    try expectHash(.@"sha3-512", "f6896cafdd752d9615f22017dfce1a281fb0598038", "\x14\x40\x6f\xd4\xa8\xbe\x27\xa8\x28\x43\x4c\xe7\xb0\x93\x5a\x50\xdf\x11\xdc\xc4\x31\x43\x7c\xac\xe0\x7f\x46\x0d\x22\x64\x97\x45\xce\x1a\xbf\xc1\xd6\x5a\x36\x1e\x9f\x3e\x08\x52\xcb\x4f\x42\x5c\x7c\x94\x27\x32\x84\x99\xd2\xa4\xe6\x99\x9b\x35\xf0\x5e\x37\x19\x59\x08");
    try expectHash(.@"sha3-512", "fe2bd8cb6ad52767eae6fbd354ceb3e72e8f55669b", "\x14\x40\x88\x8e\x30\xd4\x34\x51\xaf\xe1\x5d\x97\x11\xd6\x45\x9f\x06\x94\x71\x83\x57\x05\x86\xb3\x8b\x20\x72\x3e\x7f\x02\xfa\x47\x8c\x13\xac\x36\x3e\x07\x52\x8e\xe2\x2e\x13\x19\xa1\xcb\xfc\xb3\x29\xe6\xf6\x87\xac\xc2\x3d\x65\x0f\xf5\x99\xab\xbc\x9c\x82\x8b\x5e\xfd");
    try expectHash(.@"sha3-512", "1391551dc8f15110d256c493ed485bca8cfed07241", "\x14\x40\x89\xc6\x28\x11\xbc\x2d\x3f\xec\x81\x63\x11\x12\xad\x9f\x03\xde\x23\xd0\x65\x69\x7f\x75\x8b\x8d\xf9\xe5\xd7\x91\x47\x4a\xb2\xf2\x0d\xda\xc6\x84\xc2\x3b\x20\x3b\x73\x0b\xe4\x65\xa5\xd0\xf0\x6b\x76\xe9\xdb\xc4\x85\x90\xde\xf7\xd5\x8e\x96\xd9\x3b\x4e\x04\x18");
    try expectHash(.@"sha3-512", "2d6db2d7882fa8b7d56e74b8e24036deb475de8c94", "\x14\x40\x96\x8e\x69\x7b\x2d\x5e\x92\x47\x00\x02\xf9\xe5\x9e\x13\x55\x7f\x47\xb8\x95\xdc\x9c\x79\x08\x2a\x90\xe9\x15\x15\xf0\x25\x56\x37\x73\xae\xc0\xf7\x02\x19\xc8\x73\x50\xc7\x97\x07\xde\x67\x50\x08\x66\xd7\xfe\x08\x4c\x53\x16\xe1\x2c\x69\x30\x94\x9b\x28\x86\x5d");
    try expectHash(.@"sha3-512", "431fb5d4c9b735ba1a34d0df045118806ae2336f2c", "\x14\x40\x9a\x7a\x82\x07\xa5\x7d\x03\xe9\xc5\x24\xae\x7f\xd3\x95\x63\xbf\xe1\xa4\x66\xa3\xa0\x32\x38\x75\xeb\xa8\xb0\x34\xa1\xd5\x9c\x3b\x72\x18\x10\x35\x43\xf7\x77\x7f\x17\xef\x03\xdc\xaf\x44\xd1\x2c\x74\xdf\xb8\x37\x26\xe7\x42\x5c\xf6\x12\x25\xe9\xa5\x4b\x3b\x3a");
    try expectHash(.@"sha3-512", "b07cb1832e7e253f3eb7cf4029a82d881217d3c02f", "\x14\x40\x9b\x59\x6b\x0e\xd3\xe0\xfb\xe6\xbe\x32\x5b\x26\xe9\x4f\x83\xa9\x91\x19\xc9\x5a\xd6\xb3\x76\x11\x5c\xc2\x28\xf9\x40\x87\xa4\xca\x68\x05\x9c\x96\x41\x64\x75\x7f\x76\xe0\x8c\x89\xe5\xd5\x21\xc2\x56\xc0\x0b\x21\xaf\x19\xb0\xc8\xa5\xa0\x48\x4e\xc9\xdb\x65\xeb");
    try expectHash(.@"sha3-512", "79f28aba59f256868f8b7f868e75aedcab8b068be0", "\x14\x40\x9b\x6b\x98\x74\x9e\x9b\x03\x80\x91\xfa\x50\x53\x2e\xe3\x26\x7a\x67\xed\x36\x07\x43\x8f\x7b\xf2\x5b\x0c\x29\x29\x5f\x80\x6b\x10\xbc\x1a\x8a\xe1\x3e\x8c\x23\x74\x46\x72\xbf\x76\x28\x11\x4a\x55\x7f\xf2\x8e\xe3\xe2\xcc\xc7\x3b\x07\x59\xf4\xa9\x8a\x7f\x86\xde");
    try expectHash(.@"sha3-512", "557f47c855a5ca40daa3c0904a1e43647b4021ce0c", "\x14\x40\xa7\xab\x20\x97\x84\x59\x7d\x2e\x25\x18\x60\xe2\x46\x4c\x04\xc3\x86\xc4\x88\x7a\xf7\x78\x13\x64\x14\xc8\x0d\x7c\x64\x3c\xcd\x3b\x2a\x0b\x61\xe3\x19\x86\xa7\x9e\xf8\xe4\xfd\xd4\x16\x01\xfb\xe7\xc9\x82\xc1\x32\xdf\x4b\xc7\x7e\xcc\x2e\x27\xd3\xed\xd5\x5f\x90");
    try expectHash(.@"sha3-512", "3647da9e56b1afffa9921ce9a9caaa51ac8a166706", "\x14\x40\xcc\x1a\x0a\xcb\x60\xf1\x7e\x82\x5b\x4c\x41\xb1\xef\xf8\x4f\xc4\x2c\xf5\xdd\x2b\x44\x68\xa4\x7d\x0e\x80\x53\xff\x55\xa4\xe7\xaa\x95\xe0\x0f\xae\x8f\xc8\xea\xf2\x41\xa1\x02\x72\x75\xc8\x24\x3b\x8a\x73\x41\xb7\xfb\xd1\x87\x8a\xc3\x5e\xb0\xa8\xe1\x74\xb4\x6e");
    try expectHash(.@"sha3-512", "8a461139f4d5078f6ff77af4fbc1befc775c0e0cb0", "\x14\x40\xcd\xbf\xc2\x27\xb8\x5b\x78\x6e\xc4\x7f\x75\xbe\xa9\xc1\x9e\x7c\xaa\xf7\x0b\x8c\xbe\x61\xe0\x80\x85\x6d\xf5\x65\x0d\x4b\x2b\x6b\x67\x1c\xe9\xf6\x8d\x9c\x66\xb6\xa1\xa8\xde\x15\xd3\xc8\xd5\x97\x8a\x73\x7f\x21\xe4\xd8\x9e\x74\xd5\x98\xd9\xae\x87\xc4\x69\x4e");
    try expectHash(.@"sha3-512", "eaf89d6d7fa2ee40c7357e3d627cbd1338009570f5", "\x14\x40\xcf\xe8\x5f\x27\xf1\x8a\x9b\x31\xee\xb9\x41\xb4\xfb\x38\x5b\x22\xcb\x6c\xb7\x45\x8f\xf3\xec\xec\x23\x9d\x55\x1e\xcf\x8b\x46\x45\x65\x28\x77\xf2\x7f\xc7\x1f\xcc\x92\x78\x66\x40\x91\x43\x61\x3b\xbd\x25\x86\x31\x5c\x53\x53\x8f\x6d\xf0\x6d\x72\x0a\x44\x52\xcb");
    try expectHash(.@"sha3-512", "f7f96cf1e683e6f7e752dab4613a1b1225889bb15d", "\x14\x40\xd4\xd5\x33\xc3\x26\xe1\x57\xfb\xc5\x3e\x52\xf4\xdf\xa2\x19\xd6\x7e\x99\x8a\x8f\x54\xe0\xa8\x99\xb1\xfd\x32\xb0\x7b\xe1\x86\x35\xf3\x2c\xd8\xe9\x15\x22\x82\xa9\xd5\x2c\xe6\x47\xff\x4a\xdd\x11\xad\xff\xee\xcd\xe5\xb3\xb0\x04\x00\xfa\xc6\x8a\x08\x07\xe9\x5f");
    try expectHash(.@"sha3-512", "a244e61f124cd3590173e2809b1177ca611ff15e9a", "\x14\x40\xed\xf3\xdb\xcb\x6a\x22\x9c\x00\xd6\xaa\x35\xe9\x75\x1d\x96\x09\xab\x71\xd9\x51\x48\xa2\x8f\xb8\xfc\x9f\x3b\xf2\x6d\xf4\xc9\xed\xdc\xd4\x29\x7a\x52\x0f\x91\xd7\xa0\x29\x72\x90\x31\x97\xc9\x2c\x2d\xc5\xe1\xf8\x86\xa9\xd4\x97\xa2\xc8\x2a\x4d\x10\xd0\x63\x07");
}
